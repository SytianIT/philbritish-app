import { NgModule } from '@angular/core';
import {
    MatFormFieldModule,
    MatButtonModule,
    MatCheckboxModule,
    MatRadioModule,
    MatInputModule,
    MatSelectModule,
    MatMenuModule,
    MatDialogModule
} from '@angular/material';
import { MatDatepickerModule } from '@angular/material';
import { MatStepperModule } from '@angular/material/stepper';
import { MomentDateAdapter, MomentDateModule, MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatSnackBarModule } from '@angular/material/snack-bar';

@NgModule({
  imports: [
    MatButtonModule,
    MatCheckboxModule,
    MatRadioModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDatepickerModule,
    MatStepperModule,
    MomentDateModule,
    MatMomentDateModule,
    MatSnackBarModule,
    MatMenuModule,
    MatDialogModule
  ],
  exports: [
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatSelectModule,
    MatDatepickerModule,
    MatStepperModule,
    MomentDateModule,
    MatMomentDateModule,
    MatSnackBarModule,
    MatMenuModule,
    MatDialogModule
  ],
  declarations: [],
})
export class MyOwnCustomMaterialModuleModule { }
