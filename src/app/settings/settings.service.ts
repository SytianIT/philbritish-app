import { element } from 'protractor';
import { Injectable } from '@angular/core';
import { Headers, Response, Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { AppService } from '../app.service';

import * as _ from 'lodash';
import { Subject } from 'rxjs/Subject';

declare var jQuery: any;
@Injectable()
export abstract class SettingsService extends AppService {

    pages: any = [];
    settings: any = {};

    public howToRegister = new Subject<any>();
    isHowToRegister$ = this.howToRegister.asObservable();

    public howToClaim = new Subject<any>();
    isHowToClaim$ = this.howToClaim.asObservable();

    public faq = new Subject<any>();
    isFaq$ = this.faq.asObservable();

    public aboutUs = new Subject<any>();
    isAboutUs$ = this.aboutUs.asObservable();

    public contactUs = new Subject<any>();
    isContactUs$ = this.contactUs.asObservable();

    purchaseSlug = 'how-to-register';

    private pagesUrl = '/api/pages'; // URL to web api
    private settingsUrl = '/api/settings'; // URL to web api

    constructor(private http: Http) {
        super();
    }

    getPages(): void {
        this.http.get(this.urlPrefix + this.pagesUrl)
            .map(this.extractData)
            .toPromise()
            .then((data) => {
                this.pages = data;
            })
            .catch(this.handleError);
    }

    getThisPage(slug: string): Observable<Response> {
        return this.http.get(this.urlPrefix + '/api/' + slug + '/pages')
                        .map(this.extractData)
                        .catch(this.handleError);
    }

    getSettings(): void {
        this.http.get(this.urlPrefix + this.settingsUrl)
            .map(this.extractData)
            .toPromise()
            .then((data) => {
                this.settings = data;
            })
            .catch(this.handleError);
    }

    getObsSettings(): Observable<any> {
        return this.http.get(this.urlPrefix + this.settingsUrl)
                        .map(this.extractData)
                        .catch(this.handleError);
    }

    getInquiryEmail() {
        if (Object.keys(this.settings).length != 0) {
            return 'contact_number' in this.settings.other_data ? this.settings.other_data.contact_number : false;
        }

        return false;
    }

    getSupportNumber() {
        if (Object.keys(this.settings).length != 0) {
            return 'inquiry_email' in this.settings.other_data ? this.settings.other_data.inquiry_email : false;
        }

        return false;
    }

    getContactContent(): Observable<any> {
        return this.http.get(this.urlPrefix + '/api/get-contact')
                        .map(this.extractData)
                        .catch(this.handleError);
    }

    getPremiumPrice(): Observable<Response> {
        return this.http.get(this.urlPrefix + '/api/get-premium')
                        .map(this.extractData)
                        .catch(this.handleError);
    }

    getThisMeta(slug): Observable<Response> {
        return this.http.get(this.urlPrefix + '/api/' + slug + '/get-metas')
            .map((res: Response) => {
                let data = res.json() || [];

                jQuery('meta[name="keywords"]').attr('content', data.metaKey != null && data.metaKey != '' ? data.metaKey : 'eCTPLmo na!');
                jQuery('meta[name="description"]').attr(
                    'content',
                    data.metaDesc != null && data.metaDesc != '' ? data.metaDesc : 'Buy your CTPL at the RIGHT Price,' +
                    'Minus the HASSLE. Bago pumunta sa LTO, eCTPLmo na!'
                );
            })
            .catch(this.handleError);
    }
}
