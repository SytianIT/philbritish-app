import { Router } from '@angular/router';
import { AbstractComponent } from 'app/abstract.component';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Component, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { SnackbarService } from 'app/shared/snackbar.service';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { MyTplService } from './../../my-tpl/my-tpl.service';

import 'rxjs/add/observable/throw';
import * as _ from 'lodash';
declare var jQuery: any;

@Component({
    selector: 'app-get-quote',
    templateUrl: './get-quote.component.html',
    styleUrls: ['./get-quote.component.css']
})
export class GetQuoteComponent extends AbstractComponent implements OnInit {

    @ViewChild('quoteModal') public quoteModal: ModalDirective;

    isProcessing = false;
    isValid = true;
    captchaKey = jQuery('meta[name="g-site-key"]').attr('content');
    captchaVerify = false;

    @Output() public successQuote = new EventEmitter<boolean>();

    canProceed = false;
    policyTypes: any = [];
    vehicleTypes: any = [];
    mvTypes: any = [];

    quoteData: any = {
        renewal : true,
        years_of_coverage : 1,
        policy_type : '',
        vehicle_type : '',
        mv_type : '',
        price : 0,
        processing_fee : 0,
        total_price : 0,
    };

    quoteFormGroup: FormGroup;

    constructor(
        private router: Router,
        private formBuilder: FormBuilder,
        private snackbarService: SnackbarService,
        public myTplService: MyTplService
    ) {
        super();
     }

    ngOnInit() {
        this.myTplService.getPolicyTypes();
        this.quoteFormGroup = this.formBuilder.group({
            renewal : '',
            yearsCoverage : '',
            policyType: new FormControl('', [Validators.required]),
            vehicleType: new FormControl('', [Validators.required]),
            mvType: new FormControl('', [Validators.required]),
            totalPrice: new FormControl('', [Validators.required])
        });
    }

    get renewal() { return this.quoteFormGroup.get('renewal'); };
    get yearsCoverage() { return this.quoteFormGroup.get('yearsCoverage'); };
    get policyType() { return this.quoteFormGroup.get('policyType'); };
    get vehicleType() { return this.quoteFormGroup.get('vehicleType'); };
    get mvType() { return this.quoteFormGroup.get('mvType'); };
    get totalPrice() { return this.quoteFormGroup.get('totalPrice'); };

    show() {
        this.quoteModal.show();
    }

    hide() {
        this.quoteModal.hide();
    }

    hideReset() {
        this.resetPolicies();
        this.hide();
    }

    makeTheErrors(formControlObj, errors) {
        Object.keys(errors.messages).forEach(function(key) {
            let data = errors.key;
            let myKey = _.camelCase(key);
            if (formControlObj.get(myKey)) {
                formControlObj.get(myKey).setErrors({invalid : true});
            }
        });
    }

    resetPolicies() {
        this.vehicleTypes = [];
        this.mvTypes = [];
        this.quoteData.policy_type = '';
        this.quoteData.vehicle_type = '';
        this.quoteData.mv_type = '';
        this.resetPrices();
    }

    getVehicleType() {
        let _self = this;
        this.resetPrices();
        this.quoteData.vehicle_type = '';
        this.quoteData.mv_type = '';

        this.myTplService.getVehicleTypes(this.quoteData)
            .subscribe(data => {
                this.vehicleTypes = _self.makeThisArray(data);
                if (_self.quoteData.vehicle_type != '') {
                    setTimeout(() => {
                        _self.getMvTypes();
                    }, 500);
                }
            });
    }

    getMvTypes() {
        let _self = this;
        this.myTplService.getMvTypesAndPrice(this.quoteData)
            .subscribe(data => {
                if ('message' in data) {
                    this.canProceed = false;
                }
                this.mvTypes = _self.makeThisArray(data['mv']);
                this.quoteData.price = data['price'].price;
                this.quoteData.processing_fee = data['price'].processing_fee;
                this.quoteData.total_price = data['price'].total;
            });
    }

    resetPrices() {
        this.quoteData.price = 0;
        this.quoteData.processing_fee = 0;
        this.quoteData.total_price = 0;
    }

    goBuyNow() {
        if (this.quoteFormGroup.valid) {
            this.router.navigate([
                'app/buy-now/',
                this.quoteData.years_of_coverage,
                this.quoteData.renewal,
                this.quoteData.policy_type,
                this.quoteData.vehicle_type,
                this.quoteData.mv_type
            ]);
        }
    }
}
