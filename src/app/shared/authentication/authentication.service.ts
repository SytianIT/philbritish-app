import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { AppService } from 'app/app.service';
import { WindowrefService } from 'app/shared/windowref.service';

@Injectable()
export class AuthenticationService extends AppService {

    nativeWindow: any
    constructor(
        private http: Http,
        private winRef: WindowrefService
    ) {
        super();
        this.nativeWindow = winRef.getNativeWindow();
    }

    login(data: any): Observable<Response> {
        return this.http.post(this.urlPrefix + '/api/customer/login', data)
                        .map(this.extractData)
                        .catch(this.handleError);
    }

    signUp(data: any): Observable<Response> {
        return this.http.post(this.urlPrefix + '/api/customer/register', data)
                        .map(this.extractData)
                        .catch(this.handleError);
    }

    forgot(data: any): Observable<Response> {
        return this.http.post(this.urlPrefix + '/api/customer/reset', data)
                        .map(this.extractData)
                        .catch(this.handleError);
    }

    loginProvider(provider: string, token: string) {
        let newWindow = this.nativeWindow.open(this.urlPrefix + '/api/customer/login/' + provider + '?json_end=true&token=' + token)
    }
}
