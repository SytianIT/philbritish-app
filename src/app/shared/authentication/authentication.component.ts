import { element } from 'protractor';
import { Http } from '@angular/http';
import { AbstractComponent } from 'app/abstract.component';
import { PusherService } from './../../pusher.service';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { PasswordValidation } from './../passwordvalidation';
import { AuthenticationService } from './authentication.service';

import {
    trigger,
    state,
    style,
    animate,
    transition,
    group
} from '@angular/animations';
import 'rxjs/add/observable/throw';
import * as _ from 'lodash';
import { SnackbarService } from 'app/shared/snackbar.service';

declare var jQuery: any;

@Component({
    selector: 'app-authentication',
    templateUrl: './authentication.component.html',
    styleUrls: ['./authentication.component.css'],
    animations: [
        trigger('itemAnim', [
            state('in', style({opacity : 1})),
            transition('* => void', [
                style({opacity: '*', height: 0}),
                animate('.3s ease', style({transform: 'translateX(-100%)'})),
            ]),
            transition('void => *', [
                style({opacity: '*', height: '*'}),
                animate('.3s ease', style({transform: 'translateX(0)'})),
            ]),
        ])
    ]
})
export class AuthenticationComponent extends AbstractComponent implements OnInit {

    @ViewChild('authModal') public authModal: ModalDirective;

    isProcessing = false;
    actionTitle = 'MEMBER\'S LOGIN';
    isSignin = true;
    isSignup = false;
    isForgot = false;
    captchaKey = jQuery('meta[name="g-site-key"]').attr('content');
    csrfToken = jQuery('meta[name="csrf-token"]').attr('content');
    captchaVerify = false;

    @Output() public successLogin = new EventEmitter<boolean>();
    @Output() public successRegister = new EventEmitter<boolean>();

    login = <any> {
        email : '',
        password : ''
    };

    signup = <any> {
        first_name : '',
        last_name : '',
        contact_number : '',
        email : '',
        password : '',
        password_confirmation : '',
        captcha : '',
        sources : '',
        source_entity : ''
    };

    forgot = <any> {
        email : ''
    }

    hasForgotResponse = false;
    forgotResponse: any;

    loginFormGroup: FormGroup;
    signUpFormGroup: FormGroup;
    forgotFormGroup: FormGroup;

    signUpText = 'Sign-up';
    signInText = 'Sign-in';

    sourceOptions = [
        { name: 'Website' },
        { name: 'Facebook' },
        { name: 'Agent' },
        { name: 'Others' }
    ];
    sourceEntity = false;
    sourceEntityClass = 'col-xs-12 col-sm-12';
    sourceEntityLabel = 'Agent Name';

    constructor(
        private authentication: AuthenticationService,
        private formBuilder: FormBuilder,
        private pusherService: PusherService,
        private snackbarService: SnackbarService
    ) {
        super();
    }

    ngOnInit() {
        this.captchaVerify = false;
        // Account login
        this.pusherService.socialite.bind('socialiteSuccess', (data: any) => {
          if (this.csrfToken == data.token) {
            this.successLogin.emit(true);
            this.isProcessing = false;
            this.snackbarService.openSnackBar(data.message, 'Got it!');
          }
        });

        this.pusherService.socialite.bind('socialiteFailed', (data: any) => {
          if (this.csrfToken == data.token) {
            this.isProcessing = false;
            this.snackbarService.openSnackBar(data.message, 'Got it!');
          }
        });

        this.loginFormGroup = this.formBuilder
            .group({
                email: new FormControl('', [Validators.required, Validators.maxLength(50), Validators.email]),
                password: new FormControl('', [Validators.required, Validators.maxLength(50)])
            });

        this.signUpFormGroup = this.formBuilder
            .group({
                firstName: new FormControl('', [Validators.required, Validators.maxLength(50)]),
                lastName: new FormControl('', [Validators.required, Validators.maxLength(50)]),
                email: new FormControl('', [Validators.required, Validators.maxLength(50), Validators.email]),
                contactNumber: new FormControl('', [Validators.required, Validators.maxLength(15)]),
                password: new FormControl('', [Validators.required, Validators.maxLength(15), Validators.minLength(8)]),
                passwordConfirmation: new FormControl('', [Validators.required, Validators.maxLength(15)]),
                sources: new FormControl('', [Validators.maxLength(15)]),
                sourceEntity: new FormControl('', [Validators.maxLength(15)]),
            }, {
                validator: PasswordValidation.MatchPassword // your validation method
            });

        this.forgotFormGroup = this.formBuilder
            .group({
                email: new FormControl('', [Validators.required, Validators.maxLength(50), Validators.email])
            });
    }

    get loginEmail() { return this.loginFormGroup.get('email'); };
    get loginPassword() { return this.loginFormGroup.get('password'); };

    get signUpFirstName() { return this.signUpFormGroup.get('firstName'); };
    get signUpLastName() { return this.signUpFormGroup.get('lastName'); };
    get signUpEmail() { return this.signUpFormGroup.get('email'); };
    get signUpContactNumber() { return this.signUpFormGroup.get('contactNumber'); };
    get signUpPassword() { return this.signUpFormGroup.get('password'); };
    get signUpPasswordConfirmation() { return this.signUpFormGroup.get('passwordConfirmation'); };
    // get signUprecaptchaReactive() { return this.signUpFormGroup.get('recaptchaReactive'); }

    get forgotEmail() { return this.forgotFormGroup.get('email'); };

    show() {
        this.authModal.show();
    }

    hide() {
        this.authModal.hide();
    }

    makeTheErrors(formControlObj, errors) {
        console.log(errors.messages);
        Object.keys(errors.messages).forEach(function(key) {
            let data = errors.key;
            let myKey = _.camelCase(key);
            if (formControlObj.get(myKey)) {
                formControlObj.get(myKey).setErrors({invalid : true});
            }
        });
    }

    goSignIn() {
        this.actionTitle = 'MEMBER\'S LOGIN';
        this.isSignin = true;
        this.isSignup = false;
        this.isForgot = false;
    }

    doSignIn() {
        if (this.loginFormGroup.valid && this.isProcessing == false) {
            this.isProcessing = true;
            this.signInText = 'Processing...';
            this.authentication.login(this.login)
                .subscribe(
                    data => {
                        if ('success' in data) {
                            this.successLogin.emit(true);
                        } else {
                            this.successLogin.emit(false);
                        }
                        this.isProcessing = false;
                        this.signInText = 'Sign-in';
                    },
                    err => {
                        this.makeTheErrors(this.loginFormGroup, err);
                        this.isProcessing = false;
                        this.signInText = 'Sign-in';
                    });
        }
    }

    goSignUp() {
        this.actionTitle = 'MEMBER\'S Sign-Up';
        this.isSignup = true;
        this.isForgot = false;
        this.isSignin = false;
    }

    doSignUp() {
        if (this.signUpFormGroup.valid && this.isProcessing == false) {
            if (this.captchaVerify) {
                this.signUpText = 'Processing...';
                this.isProcessing = true;
                this.authentication.signUp(this.signup)
                    .subscribe(
                        data => {
                            if ('success' in data) {
                                this.successRegister.emit(true);
                            } else {
                                this.successRegister.emit(false);
                            }
                            this.isProcessing = false;
                            this.signUpText = 'Sign-up';
                        },
                        err => {
                            this.makeTheErrors(this.signUpFormGroup, err);
                            this.isProcessing = false;
                            this.signUpText = 'Sign-up';
                        });
            } else {
                alert('You need to confirm that you are not a robot!');
            }
        }
    }

    goForgot() {
        this.actionTitle = 'Reset Password';
        this.isForgot = true;
        this.isSignup = false;
        this.isSignin = false;
    }

    doForgot() {
        if (this.forgotFormGroup.valid && this.isProcessing == false) {
            this.isProcessing = true;
            this.authentication.forgot(this.forgot)
                .subscribe(
                    data => {
                        this.hasForgotResponse = true;
                        if ('success' in data) {
                            this.forgotResponse = data['message'];
                        }
                        this.isProcessing = false;
                    },
                    err => {
                        this.makeTheErrors(this.forgotFormGroup, err);
                        this.isProcessing = false;
                    });
        }
    }

    goProviderLogin(provider: string) {
        this.isProcessing = true;
        this.authentication.loginProvider(provider, this.csrfToken);
    }

    captchaResolved(captchaResponse: string) {
        if (captchaResponse !== null) {
            this.captchaVerify = true;
        } else {
            this.captchaVerify = false;
        }
    }

    sourcesChange() {
        if (this.signup.sources != '') {
            let selected = this.signup ? this.signup.sources : null;

            if (selected == 'Others') {
                this.sourceEntityClass = 'col-xs-12 col-sm-6';
                this.sourceEntityLabel = 'Specifics';
                this.sourceEntity = true;
            } else if (selected == 'Agent') {
                this.sourceEntityClass = 'col-xs-12 col-sm-6';
                this.sourceEntityLabel = 'Agent Name';
                this.sourceEntity = true;
            } else {
                this.sourceEntityClass = 'col-xs-12 col-sm-12';
                this.sourceEntity = false;
            }
        }
    }
}
