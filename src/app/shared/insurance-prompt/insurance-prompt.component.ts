import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-insurance-prompt',
  templateUrl: './insurance-prompt.component.html',
  styleUrls: ['./insurance-prompt.component.css']
})
export class InsurancePromptComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<InsurancePromptComponent>
  ) { }

  ngOnInit() {
  }

}
