import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from 'app/auth.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AccessGuard implements CanActivate {
    hasLogin = false;

    constructor(
        private router: Router,
        private authService: AuthService
    ) {
      this.authService.login()
          .subscribe(
          login => {
              if (login) {
                  this.hasLogin = true;
                  this.authService.updateUserData(login['data']);
                  this.authService.updateLoginState(login['result']);
              }
          });
    }

    public canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ) {
      return this.authService.login().map(e => {
        if (e) {
          return true;
        }
      }).catch(() => {
        this.router.navigate(['/login']);
        return Observable.of(false);
      });
    }

}
