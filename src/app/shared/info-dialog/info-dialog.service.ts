import { Injectable } from '@angular/core';
import { InfoDialogComponent } from './../../shared/info-dialog/info-dialog.component';
import { DialogService } from 'ng2-bootstrap-modal';

@Injectable()
export class InfoDialogService {

    title = 'Please wait...';
    message = '';
    disposable: any;

    constructor(
        private dialogService: DialogService
    ) { }

    setTitle(title) {
        this.title = title;
    }

    showConfirm() {
        this.disposable = this.dialogService.addDialog( InfoDialogComponent, {
            title: this.title,
            message: this.message})
            .subscribe((isConfirmed) => {

            });
    }

    closeConfirm() {
        this.disposable.unsubscribe();
    }
}
