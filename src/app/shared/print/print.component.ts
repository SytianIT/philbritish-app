import { ModalDirective } from 'ngx-bootstrap/modal';
import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-print',
    templateUrl: './print.component.html',
    styleUrls: ['./print.component.css']
})
export class PrintComponent implements OnInit {

    @ViewChild('printingModal') public printingModal: ModalDirective;

    @Output() public successLogin = new EventEmitter<boolean>();
    @Output() public successRegister = new EventEmitter<boolean>();

    htmlPrint: string;
    
    constructor() { }

    ngOnInit() {
    }

    show() {
        this.printingModal.show();
    }

    hide() {
        this.printingModal.hide();
    }
}
