import { SnackbarService } from 'app/shared/snackbar.service';
import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { AuthService } from './../auth.service';
import { AbstractComponent } from 'app/abstract.component';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { PasswordValidation } from './../shared/passwordvalidation';

import * as _ from 'lodash';
import { Location } from '@angular/common';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})
export class ProfileComponent extends AbstractComponent implements OnInit {

    isProcessing = false;
    hasLogin = false;
    userData: any = {
        first_name : '',
        last_name : '',
        email : '',
        customer : {
            contact_number : '',
            street : '',
            city : '',
            province : '',
            zip_code : '',
            sources : '',
            source_entity : ''
        },
        old_password : '',
        password : '',
        password_confirmation : '',
    };

    isSourceEmpty = false;
    sourceOptions = [
        { name: 'Website' },
        { name: 'Facebook' },
        { name: 'Agent' },
        { name: 'Others' }
    ];
    sourceEntity = false;
    sourceEntityClass = 'col-xs-12 col-sm-12';
    sourceEntityLabel = 'Agent Name';


    profileFormGroup: FormGroup;

    constructor(
        private authService: AuthService,
        private formBuilder: FormBuilder,
        private snackbarService: SnackbarService,
        private location: Location
    ) {
        super();
        this.authService.login()
            .subscribe(
            login => {
                if (login) {
                    this.hasLogin = true;
                    this.authService.updateUserData(login['data']);
                    this.authService.updateLoginState(login['result']);

                    if (this.userData.customer.sources == '' || this.userData.customer.sources == null) {
                        this.isSourceEmpty = true;
                    }
                }
            });

        this.authService.isLoggedInConfirmed$.subscribe(a => {
            this.hasLogin = a;
        });

        this.authService.isUserData$.subscribe(a => {
            this.userData = a;

            if (this.userData.customer.sources == '' || this.userData.customer.sources == null) {
                this.isSourceEmpty = true;
            }
        });
    }

    ngOnInit() {
        this.profileFormGroup = this.formBuilder
            .group({
                firstName: new FormControl('', [Validators.required, Validators.maxLength(50)]),
                lastName: new FormControl('', [Validators.required, Validators.maxLength(50)]),
                email: new FormControl('', [Validators.required, Validators.maxLength(50), Validators.email]),
                contactNumber: new FormControl('', [Validators.required, Validators.maxLength(15)]),
                oldPassword: new FormControl('', [Validators.maxLength(15), Validators.minLength(8)]),
                password: new FormControl('', [Validators.maxLength(15), Validators.minLength(8)]),
                passwordConfirmation: new FormControl('', [Validators.maxLength(15)]),
                street :  new FormControl('', [Validators.maxLength(50)]),
                city :  new FormControl('', [Validators.maxLength(50)]),
                province :  new FormControl('', [Validators.maxLength(50)]),
                zipCode :  new FormControl('', [Validators.maxLength(50)]),
                sources: new FormControl('', [Validators.maxLength(15)]),
                sourceEntity: new FormControl('', [Validators.maxLength(15)]),
            }, {
                validator: PasswordValidation.MatchPassword // your validation method
            });
    }

    get FirstName() { return this.profileFormGroup.get('firstName'); };
    get LastName() { return this.profileFormGroup.get('lastName'); };
    get Email() { return this.profileFormGroup.get('email'); };
    get ContactNumber() { return this.profileFormGroup.get('contactNumber'); };
    get OldPassword() { return this.profileFormGroup.get('oldPassword'); };
    get Password() { return this.profileFormGroup.get('password'); };
    get PasswordConfirmation() { return this.profileFormGroup.get('passwordConfirmation'); };
    get Sources() { return this.profileFormGroup.get('sources'); };
    get SourceEntity() { return this.profileFormGroup.get('sourceEntity'); };

    makeTheErrors(formControlObj, errors) {
        Object.keys(errors.messages).forEach(function(key) {
            let data = errors.key;
            let myKey = _.camelCase(key);
            if (formControlObj.get(myKey)) {
                formControlObj.get(myKey).setErrors({invalid : true});
            }
        });
    }

    doUpdate() {
        if (this.profileFormGroup.valid && this.isProcessing == false) {
            this.isProcessing = true;
            this.authService.updateProfile(this.userData)
                .subscribe(
                    data => {
                        this.isProcessing = false;
                        this.userData = data;
                        this.snackbarService.openSnackBar('Profile updated.', 'Got it!');
                    },
                    err => {
                        this.makeTheErrors(this.profileFormGroup, err);
                        this.isProcessing = false;
                    });
        }
    }

    goBack() {
        this.location.back();
    }

    getMyError(formKey: number, formName: string, validator: string) {
        if (this.profileFormGroup.controls.formName.hasError(validator)) {
            return true;
        }

        return false;
    }

    sourcesChange() {
        if (this.userData.customer.sources != '') {
            let selected = this.userData.customer.sources;

            if (selected == 'Others') {
                this.sourceEntityClass = 'col-xs-12 col-sm-6';
                this.sourceEntityLabel = 'Specifics';
                this.sourceEntity = true;
            } else if (selected == 'Agent') {
                this.sourceEntityClass = 'col-xs-12 col-sm-6';
                this.sourceEntityLabel = 'Agent Name';
                this.sourceEntity = true;
            } else {
                this.sourceEntityClass = 'col-xs-12 col-sm-12';
                this.sourceEntity = false;
            }
        }
    }
}
