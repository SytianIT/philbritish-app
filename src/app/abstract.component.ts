import { Component } from '@angular/core';

import * as _moment from 'moment';
const moment = _moment;
declare var jQuery: any;

export abstract class AbstractComponent {

    getSurveyID() {
        return window['surveyId'];
    }

    successNotification(message: string) {
        jQuery.jGrowl(message, {
            theme: 'bg-green',
            position: 'top-right',
            life: 3000,
            click: function(e: any, m: any, o: any) {
                // close notification
                // console.log(e, m, o, 'notification click');
            }
        });
    }

    errorNotification(message: string) {
        jQuery.jGrowl(message, {
            theme: 'bg-red',
            position: 'top-right',
            life: 5000
        });
        this.forceUpdate();
    }

    forceUpdate() {
        setTimeout(() => {
            document.getElementById('ngApp').click();
        });
    }

    makeThisArray(myObj) {
        let arr = Object.keys(myObj).map(function(k) {
            return myObj[k];
        });

        return arr;
    }

    formatMoment(dateString, format) {
        let momentDate = moment(dateString);

        return momentDate.format(format);
    }
}
