import { routerTransition } from 'app/shared/router.animations';
import { SnackbarService } from './../shared/snackbar.service';
import { AbstractComponent } from 'app/abstract.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { SettingsService } from 'app/settings/settings.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AuthService } from 'app/auth.service';
import { AuthenticationComponent } from 'app/shared/authentication/authentication.component';
import { GetQuoteComponent } from 'app/shared/get-quote/get-quote.component';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css'],
    // animations: [routerTransition()],
    // host: {'[@routerTransition]': ''}
})
export class HomeComponent extends AbstractComponent implements OnInit {

    @ViewChild('authModal') public authModal: AuthenticationComponent;
    @ViewChild('quoteModal') public quoteModal: GetQuoteComponent;

    userData: any = {};
    hasAuth = false;
    successLogin = false;
    successRegister = false;
    snackBarAction = 'Got it!';
    isLoading = true;

    videoUrl = '';
    imageUrl = '';
    homeIntroText: any = {};
    hasVideo = true;

    metaKey = '';
    metaDesc = '';

    constructor(
        private router: Router,
        private settingsService: SettingsService,
        private domSanitizer: DomSanitizer,
        public authService: AuthService,
        private snackbarService: SnackbarService
    ) {
        super();
        this.authService.isLoggedInConfirmed$.subscribe(a => {
          console.log(a);
          this.hasAuth = a;
          this.successLogin = a;
          this.successRegister = a;
        });

        this.authService.isUserData$.subscribe(a => {
            this.userData = a;
        });
    }

    ngOnInit() {
        this.getMetas();

        this.settingsService.getObsSettings()
            .subscribe(
                data => {
                    if (data.youtube_id == null || data.youtube_id == '') {
                        this.hasVideo = false;
                    }
                    this.imageUrl = data.full_logo_home;
                    this.videoUrl = 'http://www.youtube.com/embed/' + data.youtube_id;
                    if (data.other_data) {
                        this.homeIntroText = data.other_data.home_introduction;
                    }
                    this.isLoading = false;
            });

        this.authService.login()
            .subscribe(
            login => {
                if (login) {
                    this.authService.updateUserData(login['data']);
                    this.authService.updateLoginState(login['result']);
                }
            });
    }

    getMetas() {
        this.settingsService.getThisMeta('home')
            .subscribe(data => {});
    }

    goToPurchase() {
        this.router.navigate(['app/how-to-register']);
    }

    goCreate() {
        if (this.hasAuth) {
            this.router.navigate(['app/account/my-tpl-insurance/new']);
        } else {
            this.router.navigate(['app/buy-now']);
        }
    }

    showQuoteModal() {
        this.quoteModal.show();
    }

    // For Authentication Process
    showAuthModal() {
        if (this.successLogin == false && this.successRegister == false && this.hasAuth == false) {
            this.authModal.show();
        } else {
            this.snackbarService.openSnackBar(
                'You are already logged-in, please refresh the page to update your instance.',
                this.snackBarAction
            );
        }
    }

    hideAuthModal() {
        this.authModal.hide();
    }

    onSuccessQuote(result: boolean) {
    }

    onSuccessLogin(result: boolean) {
        this.successLogin = result;
        if (result) {
            this.snackbarService.openSnackBar('Login successfull.', this.snackBarAction);
            this.successAuth(result);
        } else {
            this.doFailHumanResponse();
        }
    }

    onSuccessRegister(result: boolean) {
        this.successRegister = result;
        if (result) {
            this.snackbarService.openSnackBar('Registration successfull', this.snackBarAction);
            this.successAuth(result);
        } else {
            this.doFailHumanResponse();
        }
    }

    doFailHumanResponse() {
        this.snackbarService.openSnackBar('Crap! Something went wrong, please try again later.', this.snackBarAction);
    }

    successAuth(result) {
        let _self = this;
        this.hideAuthModal();

        // Continue process
        setTimeout(function() {
            _self.authService.login()
                .subscribe(
                login => {
                    if (login) {
                        _self.authService.updateUserData(login['data']);
                        _self.authService.updateLoginState(login['result']);
                    }
                });
        }, 500);
    }

}
