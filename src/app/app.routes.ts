import { ShowTplComponent } from './my-tpl/show-tpl/show-tpl.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PageComponent } from './page/page.component';
import { AppComponent } from 'app/app.component';
import { MyTplComponent } from 'app/my-tpl/my-tpl.component';
import { NewTplComponent } from './my-tpl/new-tpl/new-tpl.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from 'app/profile/profile.component';
import { AccessGuard } from 'app/shared/access.guard';

export const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'home', component: HomeComponent },
    { path: 'app/buy-now', component: NewTplComponent },
    { path: 'app/buy-now/:years/:renewal/:policyType/:vehicleType/:mvType', component: NewTplComponent },
    { path: 'app/account/profile', component: ProfileComponent, canActivate: [AccessGuard] },
    { path: 'app/account/my-tpl-insurance', component: MyTplComponent, canActivate: [AccessGuard] },
    { path: 'app/account/my-tpl-insurance/new', component: NewTplComponent, canActivate: [AccessGuard] },
    { path: 'app/account/my-tpl-insurance/reprocess/:id', component: NewTplComponent, canActivate: [AccessGuard] },
    { path: 'app/account/my-tpl-insurance/show/:id', component: ShowTplComponent, canActivate: [AccessGuard] },

    { path: 'app/:slug', component: PageComponent },
];
