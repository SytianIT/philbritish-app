import { routerTransition } from './shared/router.animations';
import { Location } from '@angular/common';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from './auth.service';
import { SettingsService } from './settings/settings.service';
import { AbstractComponent } from 'app/abstract.component';
import { RouterOutlet } from '@angular/router/src/directives/router_outlet';

import { MatMenuTrigger } from '@angular/material';

declare var jQuery: any;

@Component({
    selector: 'philbritish-app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    // animations: [routerTransition()],
    // host: {'[@routerTransition]': ''}
})
export class AppComponent extends AbstractComponent implements OnInit {

    @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;

    title = 'CTPL Application Dashboard';

    baseUri = '';

    account: any = [];
    pages: any = [];
    settings: any = [];
    hasLogin = false;
    userData: any = [];

    logoPath = '';
    acctProfilePath = '';

    baseUrl =  jQuery('meta[name="dashboard-url"]').attr('content');
    logoutUrl =  jQuery('meta[name="logout-url"]').attr('content');
    path: string;

    private errorMessage: any = '';

    constructor (
        private router: Router,
        public settingsService: SettingsService,
        public authService: AuthService,
        public location: Location
    ) {
        super();

        this.authService.isLoggedInConfirmed$.subscribe(a => {
            this.hasLogin = a;
        });

        this.authService.isUserData$.subscribe(a => {
            this.userData = a;
        });
    }

    ngOnInit(): void {
        this.baseUri = this.authService.getBaseUri();
        this.authService.login()
            .subscribe(
            login => {
                if (login) {
                    this.hasLogin = true;
                    this.authService.updateUserData(login['data']);
                    this.authService.updateLoginState(login['result']);
                }
            });

        this.settingsService.getPages();
        this.settingsService.getSettings();
    }

    goLogout() {
        this.authService.logout()
        .subscribe(
            logout => {
                if (logout) {
                    this.hasLogin = false;
                    this.authService.updateUserData({});
                    this.authService.updateLoginState(false);
                    this.router.navigate(['/']);
                }
            });
    }

    private routeChanged(r: any): void {
        this.path = r.url.replace('/', '');
    }

    getRouteAnimation(outlet) {
        return outlet.activatedRouteData.animation
    }
}
