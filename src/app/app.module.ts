import { AccessGuard } from './shared/access.guard';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { PageComponent } from './page/page.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_BASE_HREF } from '@angular/common';
import { Http, HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
// import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { ModalModule } from 'ngx-bootstrap';

import { MyOwnCustomMaterialModuleModule } from './my-own-custom-material-module/my-own-custom-material-module.module';

import { RecaptchaModule } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';

import { AuthenticatedHttpService } from './authenticated-http.service';
import { SweetAlertService } from 'ng2-sweetalert2';

import { routes } from './app.routes';
import { AuthService } from 'app/auth.service';
import { SettingsService } from 'app/settings/settings.service';
import { MyTplComponent } from './my-tpl/my-tpl.component';
import { HomeComponent } from './home/home.component';
import { MyTplService } from 'app/my-tpl/my-tpl.service';
import { NewTplComponent } from './my-tpl/new-tpl/new-tpl.component';
import { PusherService } from 'app/pusher.service';
import { WindowrefService } from './shared/windowref.service';
import { SnackbarService } from 'app/shared/snackbar.service';
import { InfoDialogComponent } from './shared/info-dialog/info-dialog.component';
import { InfoDialogService } from 'app/shared/info-dialog/info-dialog.service';
import { ShowTplComponent } from './my-tpl/show-tpl/show-tpl.component';
import { AuthenticationComponent } from './shared/authentication/authentication.component';
import { AuthenticationService } from 'app/shared/authentication/authentication.service';
import { ProfileComponent } from './profile/profile.component';
import { GetQuoteComponent } from './shared/get-quote/get-quote.component';
import { SafeUrlPipe } from './pipe/safe-url.pipe';
import { SafeHtmlPipe } from './pipe/safe-html.pipe';
import { SafeStylePipe } from './pipe/safe-style.pipe';
import { ClaimFormComponent } from './page/partials/claim-form/claim-form.component';
import { ClaimService } from 'app/page/partials/claim-form/claim.service';
import { InquiryFormComponent } from './page/partials/inquiry-form/inquiry-form.component';
import { InquiryService } from 'app/page/partials/inquiry.service';
import { IframeAutoHeightDirective } from './directive/iframe-auto-height.directive';
import { EqualizeDirective } from './directive/equalize.directive';
import { PrintComponent } from './shared/print/print.component';
import { InsurancePromptComponent } from './shared/insurance-prompt/insurance-prompt.component';

@NgModule({
    declarations: [
        AppComponent,
        PageComponent,
        MyTplComponent,
        HomeComponent,
        NewTplComponent,
        InfoDialogComponent,
        ShowTplComponent,
        AuthenticationComponent,
        ProfileComponent,
        GetQuoteComponent,
        SafeUrlPipe,
        SafeHtmlPipe,
        SafeStylePipe,
        ClaimFormComponent,
        InquiryFormComponent,
        IframeAutoHeightDirective,
        EqualizeDirective,
        PrintComponent,
        InsurancePromptComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MyOwnCustomMaterialModuleModule,
        HttpClientModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot(routes), // , {useHash: true}
        ModalModule.forRoot(),
        RecaptchaModule.forRoot(), // Keep in mind the "forRoot"-magic nuances!
        RecaptchaFormsModule
    ],
    entryComponents: [
        InfoDialogComponent,
        InsurancePromptComponent
    ],
    providers: [
        MyTplService,
        SettingsService,
        AuthService,
        PusherService,
        SnackbarService,
        WindowrefService,
        InfoDialogService,
        AuthenticationService,
        ClaimService,
        InquiryService,
        AccessGuard,
        // {
        //     provide: Http,
        //     useClass: AuthenticatedHttpService
        // },
        SweetAlertService
    ],
    bootstrap: [AppComponent],
    exports: [MyTplComponent],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
