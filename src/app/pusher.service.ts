import { environment } from './../environments/environment.prod';
import { Injectable, Inject } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';

declare var jQuery: any;
declare const Pusher: any;

@Injectable()
export class PusherService {
    pusher: any;
    loginChannel: any;
    socialLoginChannel: any;
    insuranceChannel: any;
    paymentChannel: any;
    cocafChannel: any;
    failChannel: any;
    socialite: any;

    constructor() {
        let token: any = jQuery('meta[name="csrf-token"]').attr('content');
        let base = jQuery('meta[name="base-url"]').attr('content');
        // f8227b577a4419f04f88 - development
        // 3eb0e13694fc2c1b82c2 - production
        // c7ce46af6dda3382a145 - staging
        let pusherKey = environment.production ? '3eb0e13694fc2c1b82c2' : 'c7ce46af6dda3382a145';
        this.pusher = new Pusher(pusherKey,
            {
                cluster: 'ap1',
                encrypted: true,
                authEndpoint: base,
                auth: {
                    headers: {
                        'X-CSRF-Token': token
                    }
                }
            }
        );

        this.socialite = this.pusher.subscribe('socialite');
        this.loginChannel = this.pusher.subscribe('login');
        this.socialLoginChannel = this.pusher.subscribe('social-login');
        this.insuranceChannel = this.pusher.subscribe('insurance');
        this.paymentChannel = this.pusher.subscribe('payment');
        this.cocafChannel = this.pusher.subscribe('cocaf');
        this.failChannel = this.pusher.subscribe('fail');
    }
}
