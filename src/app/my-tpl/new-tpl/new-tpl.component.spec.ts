import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewTplComponent } from './new-tpl.component';

describe('NewTplComponent', () => {
  let component: NewTplComponent;
  let fixture: ComponentFixture<NewTplComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewTplComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewTplComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
