import { InsurancePromptComponent } from './../../shared/insurance-prompt/insurance-prompt.component';
import { AuthenticationComponent } from './../../shared/authentication/authentication.component';
import { AuthService } from './../../auth.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { SnackbarService } from './../../shared/snackbar.service';
import { AbstractComponent } from 'app/abstract.component';
import { CurrencyPipe, Location } from '@angular/common';
import { MyTplService } from './../my-tpl.service';
import { FormArray } from '@angular/forms/src/model';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { PusherService } from 'app/pusher.service';

import { ModalDirective } from 'ngx-bootstrap/modal';

import Inputmask from 'inputmask';
import * as _moment from 'moment';
import { setTimeout } from 'timers';
import { MatStepper, MatDialog } from '@angular/material';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { ISubscription } from 'rxjs/Subscription';
const moment = _moment;
declare var jQuery: any;

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-new-tpl',
  templateUrl: './new-tpl.component.html',
  styleUrls: ['./new-tpl.component.css']
})
export class NewTplComponent extends AbstractComponent implements OnInit, AfterViewInit, OnDestroy {

  private subscription: ISubscription;

  baseUri = '';
  preMadeFields = false;
  isRenewing = false;
  isPending = false;

  homeUrl =  jQuery('meta[name="home-url"]').attr('content');
  hasAuth = false;
  authData: any = [];
  successLogin = false;
  successRegister = false;
  snackBarAction = 'Got it!';

  @ViewChild('haltButton') haltButton: ElementRef;
  @ViewChild('procsModal') public procsModal: ModalDirective;
  @ViewChild('authModal') public authModal: AuthenticationComponent;
  @ViewChild('matHorizontalStepper') stepper: MatStepper;

  modalTitle = 'Processing request, please dont close this window.';
  modalInfoConf = {
    keyboard: false,
    ignoreBackdropClick: true
  };

  showId: any;

  isFailCocaf = false;
  isLoading = false;
  canProceed = true;
  paymentDone = false;
  registrationDone = false;
  isLinear = true;
  policyTypes: any = [];
  vehicleTypes: any = [];
  mvTypes: any = [];
  generatedUrl = '';

  formGroup: FormGroup;

  insuranceData = <any> {
    id : null,
    transaction_reference : '',
    renewal : true,
    years_of_coverage : 1,
    policy_type : '',
    vehicle_type : '',
    mv_type : '',
    mv_file_number : '',
    plate : '',
    engine_number : '',
    chassis_number : '',
    make : '',
    body_type : '',
    model : '',
    color : '',
    weight : '',
    capacity : '',
    assured_name : '',
    house_number : '',
    bldg : '',
    street : '',
    barangay : '',
    province : '',
    municipality_city : '',
    zip_code : '',
    assured_contact_no : '',
    assured_email : '',
    assured_tin : '',
    inception_date_from : moment(), // .format('MMMM D, YYYY'),
    inception_date_to : moment().add(1, 'years'), // .format('MMMM D, YYYY')
    price : 0,
    processing_fee : 0,
    total_price : 0,
    exists : true,
    existing_insurance : false,
    is_fail : false,
    fail_message : '',
    policy_number : '',
    renewal_policy_no : ''
  };

  savedData: any = {
    insurance: [],
    canproceed: false
  };

  get formArray(): AbstractControl | null { return this.formGroup.get('formArray'); }

  constructor(
    private router: Router,
    public myTplService: MyTplService,
    private _formBuilder: FormBuilder,
    private pusherService: PusherService,
    private authService: AuthService,
    private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private dialog: MatDialog
  ) {
    super();
    this.subscription = this.authService.login()
    .subscribe(
      login => {
        if (login) {
          this.hasAuth = true;
          this.authService.updateUserData(login['data']);
          this.authService.updateLoginState(login['result']);
        }
      });
      this.authService.isLoggedInConfirmed$.subscribe(a => {
        this.hasAuth = a;
      });
      this.authService.isUserData$.subscribe(a => {
        this.authData = a;
      });
    }

    ngOnInit() {
      this.openDialog();

      this.baseUri = jQuery('meta[name="base-url"]').attr('content');
      this.pusherService.failChannel.bind('failRetrievingTransaction', (data: any) => {
        // console.log(data); // <------ Comment this once Productions
        if (data.id == this.authData.id) {
          this.failedModal();
          this.snackbarService.openSnackBar(data.message, 'Got it!');
        }
      });

      this.pusherService.insuranceChannel.bind('insuranceCreated', (data: any) => {
        // console.log(data); // <------ Comment this once Productions
        if (data.id == this.authData.id) {
          // If payment already exist, start cocaf instead
          if ('payment' in data.insurance && data.insurance.payment && data.insurance.payment.status == 'S') {
            this.paymentDone = true;
            this.modalTitle = 'Finishing registration, please dont close this window.';
            this.updateInsuranceData(data.insurance);
            this.doCocafRegistration(data);
          } else {
            this.modalTitle = 'Processing payment, please dont close this window.';
            this.updateInsuranceData(data.insurance);
            this.processPayment(data);
          }
        }
      });

      // Pusher for Payment Post Back
      this.pusherService.paymentChannel.bind('paymentPostBack', (data: any) => {
        // console.log(data); // <------ Comment this once Productions
        if (data.id == this.authData.id) {
          this.generatedUrl = '';
          this.paymentDone = true;
          this.modalTitle = 'Finishing registration, please dont close this window.';
          this.updateInsuranceData(data.insurance);

          if (data.payment.status == 'S') {
            this.doCocafRegistration(data);
          } else if (data.payment.status == 'P') {
            let _self = this;
            this.registrationDone = true;
            this.modalTitle = 'Registration is on-hold, waiting for payment.';
            setTimeout(function() {
              _self.router.navigate(['app/account/my-tpl-insurance']);
            }, 5000)
          }
        }
      });

      this.pusherService.paymentChannel.bind('paymentPostBackError', (data: any) => {
        // console.log(data); // <------ Comment this once Productions
        if (data.id == this.authData.id) {
          this.failedModal();
          this.generatedUrl = '';
          this.snackbarService.openSnackBar(data.message, 'Got it!');
          this.updateInsuranceData(data.insurance);
          this.canProceed = false;
        }
      });
      this.pusherService.paymentChannel.bind('paymentPostBackInvalid', (data: any) => {
        // console.log(data); // <------ Comment this once Productions
        if (data.id == this.authData.id) {
          this.generatedUrl = '';
          this.snackbarService.openSnackBar(data.message, 'Got it!');
          this.updateInsuranceData(data.insurance);
          this.failedModal();
        }
      });

      // Pusher for COCAF Registration
      this.pusherService.cocafChannel.bind('cocafFailedRegistered', (data: any) => {
        // console.log(data); // <------ Comment this once Productions
        if (data.id == this.authData.id) {
          this.snackbarService.openSnackBar(data.message, 'Got it!');
          this.modalTitle = 'LTO Registration Failed...';
          this.updateInsuranceData(data.insurance);
          this.failedModal();
        }
      });
      this.pusherService.cocafChannel.bind('cocafRegistered', (data: any) => {
        // console.log(data); // <------ Comment this once Productions
        if (data.id == this.authData.id) {
          this.registrationDone = true;
          this.modalTitle = 'Registration Success!';
          this.updateInsuranceData(data.insurance);
          let _self = this;
          setTimeout(function() {
            _self.router.navigate(['app/account/my-tpl-insurance']);
          }, 1000)
        }
      });

      // Initialize Form Steppers
      this.formGroup = this._formBuilder.group({
        formArray: this._formBuilder.array([
          this._formBuilder.group({
            renewal : '',
            yearsCoverage : '',
            policyType: new FormControl('', [Validators.required]),
            vehicleType: new FormControl('', [Validators.required]),
            mvType: new FormControl('', [Validators.required]),
            totalPrice: new FormControl('', [Validators.required])
          }),
          this._formBuilder.group({
            mvFileNumber: new FormControl('', [Validators.required, Validators.maxLength(15)]),
            plate: new FormControl('', [Validators.required, Validators.maxLength(8)]),
            engineNumber : new FormControl('', [Validators.required, Validators.maxLength(40)]),
            chassisNumber : new FormControl('', [Validators.required, Validators.maxLength(40)]),
          }),
          this._formBuilder.group({
            make : new FormControl('', [Validators.required, Validators.maxLength(25)]),
            bodyType : new FormControl('', [Validators.required, Validators.maxLength(30)]),
            model : new FormControl('', [Validators.required, Validators.maxLength(25)]),
            color : new FormControl('', [Validators.required, Validators.maxLength(25)]),
            weight : new FormControl('', Validators.required),
            capacity : new FormControl('', Validators.required)
          }),
          this._formBuilder.group({
            assuredName : new FormControl('', [Validators.required, Validators.maxLength(100), Validators.minLength(4)]),
            // assuredAddress : new FormControl('', [Validators.required, Validators.maxLength(100)]),
            houseNumber : new FormControl('', [Validators.maxLength(15)]),
            bldg : new FormControl('', [Validators.maxLength(25)]),
            street : new FormControl('', [Validators.required, Validators.maxLength(25)]),
            barangay : new FormControl('', [Validators.required, Validators.maxLength(50)]),
            province : new FormControl('', [Validators.required, Validators.maxLength(50)]),
            municipalityCity : new FormControl('', [Validators.required, Validators.maxLength(50)]),
            assuredContactNumber : new FormControl('', [Validators.required, Validators.maxLength(20)]),
            assuredEmail : new FormControl('', [Validators.required, Validators.maxLength(50), Validators.email]),
            assuredTin: new FormControl('', [Validators.maxLength(15)]), // Validators.required
            inceptionDateFrom : new FormControl(this.insuranceData.inception_date_from, Validators.required),
            inceptionDateTo : new FormControl({
              value: this.insuranceData.inception_date_to, disabled: true
            }, Validators.required)
          }),
          this._formBuilder.group({
          })
        ])
      });

      this.myTplService.getPolicyTypes();

      this.activatedRoute.params.subscribe((params: Params) => {
        let _self = this;
        let years = params['years'];
        let renewal = params['renewal'];
        let mvType = params['mvType'];
        let policyType = params['policyType'];
        let vehicleType = params['vehicleType'];

        if (years && renewal && mvType && policyType && vehicleType) {
          this.preMadeFields = true;
          this.insuranceData.years_of_coverage = parseInt(years, 0);
          this.insuranceData.renewal = true;
          this.insuranceData.policy_type = policyType;
          this.insuranceData.vehicle_type = parseInt(vehicleType, 0);
          this.insuranceData.mv_type = mvType;
          this.getVehicleType();
          this.preMadeFields = false;
        }

        let id = params['id'];
        if (id) {
          this.myTplService.getThisInsurance(id)
          .subscribe(data => {
            this.makeRenewingData(data);
          }, err => {
            this.snackbarService.openSnackBar('Something wrong during retrieving of your insurance!', 'Got It!');
            setTimeout(function() {
              _self.location.back();
            }, 2000);
          });
        }
      });
    }

    ngOnDestroy() {
      this.pusherService.cocafChannel.unbind();
      this.pusherService.failChannel.unbind();
      this.pusherService.insuranceChannel.unbind();
      this.pusherService.paymentChannel.unbind();
      this.subscription.unsubscribe();
    }

    ngAfterViewInit() {
      let phoneNumberInput = document.getElementById('assured-tin');
      let inputmask = new Inputmask('999-999-999-999');
      // todo Comment the next line to make work app
      inputmask.mask(phoneNumberInput);

      if (this.isRenewing) {
        this.stepper.selectedIndex = 4;
      }
    }

    formatDate(dateString, format) {
      return dateString ? moment(dateString).format(format) : moment().format(format);
    }

    openDialog() {
      setTimeout(() => {
        const currentDialog = this.dialog.open(InsurancePromptComponent, {
          maxHeight: '300px',
          maxWidth: '600px',
          disableClose: true,
          closeOnNavigation: true
        });

        currentDialog.afterClosed().subscribe(result => {
          if (!result) {
            if (this.hasAuth == false) {
              this.router.navigate(['']);
            } else {
              this.router.navigate(['app/account/my-tpl-insurance']);
            }
          }
        });
      }, 1500);
    }

    failedModal() {
      let _self = this;
      this.modalTitle = 'Failed...';
      setTimeout(function() {
        _self.procsModal.hide();
        _self.haltProcess();
      }, 1000);
    }

    makeRenewingData(data) {
      if (data.status == 'completed' || data.status == 'unpaid') {
        this.preMadeFields = true;
        this.insuranceData = data;

        this.insuranceData.renewal = true;
        this.insuranceData.years_of_coverage = parseInt(data.years_of_coverage, 0);
        this.insuranceData.vehicle_type = parseInt(data.vehicle_type, 0);
        this.insuranceData.inception_date_from = moment(data.inception_date_to).add(1, 'years');
        this.insuranceData.inception_date_to = moment(data.inception_date_from).add(1, 'years');
        this.insuranceData.renewal_policy_no = data.policy_number;

        this.isRenewing = true;
        this.insuranceData.id = null;
        this.insuranceData.payment = null;
        this.insuranceData.cocaf = null;
        this.getVehicleType();
      } else if (data.status == 'incomplete') {
        this.preMadeFields = true;
        this.insuranceData = data;

        this.isPending = true;
        if (data.is_fail == 1) {
          this.isFailCocaf = true;
          this.insuranceData.is_fail = true;
        } else {
          this.isFailCocaf = false;
          this.insuranceData.is_fail = false;
        }

        this.insuranceData.message_fail = data.message_fail;
        this.insuranceData.renewal = data.registration_type == 'R' ? true : false;
        this.insuranceData.years_of_coverage = parseInt(data.years_of_coverage, 0);
        this.insuranceData.vehicle_type = parseInt(data.vehicle_type, 0);
        this.insuranceData.inception_date_from = moment(data.inception_date_from);
        this.insuranceData.inception_date_to = moment(data.inception_date_to);
        this.getVehicleType();
      }
    }

    resetPolicies() {
      this.vehicleTypes = [];
      this.mvTypes = [];
      this.insuranceData.policy_type = '';
      this.insuranceData.vehicle_type = '';
      this.insuranceData.mv_type = '';
      this.adjustInceptionTo();
      this.resetPrices();
    }

    getVehicleType() {
      let _self = this;
      if (this.preMadeFields == false) {
        this.insuranceData.vehicle_type = '';
        this.insuranceData.mv_type = '';
      }
      if (_self.insuranceData.policy_type != '') {
        this.resetPrices();
        this.myTplService.getVehicleTypes(this.insuranceData)
        .subscribe(data => {
          this.vehicleTypes = _self.makeThisArray(data);
          if (_self.insuranceData.vehicle_type != '') {
            _self.getMvTypes();
          }
        });
      }
    }

    getMvTypes() {
      let _self = this;
      this.myTplService.getMvTypesAndPrice(this.insuranceData)
      .subscribe(data => {
        if ('message' in data) {
          this.canProceed = false;
        }
        this.mvTypes = _self.makeThisArray(data['mv']);
        this.insuranceData.price = data['price'].price;
        this.insuranceData.processing_fee = data['price'].processing_fee;
        this.insuranceData.total_price = data['price'].total;
      });
    }

    resetPrices() {
      this.insuranceData.price = 0;
      this.insuranceData.processing_fee = 0;
      this.insuranceData.total_price = 0;
    }

    formatTin() {
      let tin = this.insuranceData.assured_tin,
      length = tin.length,
      newVal = '';

      for (let index = 0; index < length; index++) {
        let char = tin.charAt(index);
        if (char == '-') {
          newVal += '-';
        } else if (char == '_') {
          newVal += 0;
        } else {
          newVal += char;
        }
      }

      this.insuranceData.assured_tin = newVal;
    }

    initTin() {
      if (this.insuranceData.assured_tin == '') {
        this.insuranceData.assured_tin = '000-000-000-000';
      }
    }

    goSavingData() {
      alert('test');
    }

    getMyError(formKey: number, formName: string, validator: string) {
      if (this.formGroup.controls.formArray['controls'][formKey]['controls'][formName].hasError(validator)) {
        return true;
      }

      return false;
    }

    adjustInceptionTo() {
      let increase = 1;
      if (this.insuranceData.years_of_coverage == '3' || this.insuranceData.years_of_coverage == 3) {
        increase = 3;
      }

      let fromDate = moment(this.insuranceData.inception_date_from).add(increase, 'years');
      this.insuranceData.inception_date_to = fromDate;
    }

    getSelectedObj(object, id, label = null) {
      let myObj = null;
      object.forEach(element => {
        if (element.id == id ) {
          if (label != null) {
            myObj = element.label;
          }
        }
      });

      return myObj;
    }

    updateInsuranceData(data) {
      this.insuranceData.exists = true;
      this.insuranceData.id = data.id;
    }

    haltProcess() {
      this.isLoading = false;
    }

    showConfirm() {
      this.procsModal.show();
    }

    startProcessing() {
      if (this.hasAuth == false) {
        this.showAuthModal();

        return false;
      } else {
        if (this.canProceed) {
          this.isLoading = true;
          this.modalTitle = 'Processing request, please dont close this window.';
          this.procsModal.show();

          let rawData = this.insuranceData;
          rawData.inception_date_from = moment(rawData.inception_date_from).format('L');
          rawData.inception_date_to = moment(rawData.inception_date_to).format('L');

          this.myTplService.createInsurance(rawData)
          .subscribe(data => this.savedData = data);

          return false;
        }
      }

      this.snackbarService.openSnackBar('Purchase cannot proceed, please try again later!', 'Got it!');
    }

    processPayment(data) {
      if (this.canProceed) {
        let event = new MouseEvent('click', { bubbles: true });

        this.generatedUrl = this.myTplService.doPayment(data);
        setTimeout(() => {
          document.getElementById('haltButton').click();
          // window.open(this.generatedUrl, '_blank');
          // this.haltButton.nativeElement.dispatchEvent(event);
        }, 500);
      }
    }

    doCocafRegistration(data) {
      if (this.paymentDone) {
        this.myTplService.cocafRegister(data);
      }
    }

    showAuthModal() {
      if (this.successLogin == false && this.successRegister == false && this.hasAuth == false) {
        this.authModal.show();
      }
    }

    hideAuthModal() {
      this.authModal.hide();
    }

    onSuccessLogin(result: boolean) {
      this.successLogin = result;
      if (result) {
        this.snackbarService.openSnackBar('Login successfull, please wait as we continue the process.', this.snackBarAction);
        this.successAuth(result);
      } else {
        this.snackbarService.openSnackBar('Darn! Something went wrong, please try again later.', this.snackBarAction);
      }
    }

    onSuccessRegister(result: boolean) {
      this.successRegister = result;
      if (result) {
        this.snackbarService.openSnackBar('Registration successfull, please wait as we continue the process', this.snackBarAction);
        this.successAuth(result);
      } else {
        this.snackbarService.openSnackBar('Crap! Something went wrong, please try again later.', this.snackBarAction);
      }
    }

    successAuth(result) {
      let _self = this;
      this.hideAuthModal();

      // Continue process
      setTimeout(function() {
      _self.authService.login()
      .subscribe(
        login => {
          if (login) {
            _self.authService.updateUserData(login['data']);
            _self.authService.updateLoginState(login['result']);
            _self.startProcessing();
          }
        });
      }, 500);
    }

    goBack() {
      this.location.back();
    }
  }
