import { Injectable, OnInit } from '@angular/core';
import { Headers, Response, Http, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { AppService } from 'app/app.service';
import { WindowrefService } from 'app/shared/windowref.service';
import { environment } from './../../environments/environment';
import { dragonpay } from './../../assets/config';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

declare var jQuery: any;
declare const sha1: any;

@Injectable()
export class MyTplService extends AppService {

    public headers = new Headers({
        'Content-Disposition' : 'attachment'
    });
    downloadHeaders = new RequestOptions({ headers: this.headers });

    policyTypes: any = [];
    nativeWindow: any

    fetching = true;

    constructor(
        private http: Http,
        private winRef: WindowrefService
    ) {
        super();
        this.nativeWindow = winRef.getNativeWindow();
    }

    getInsurance(term): Observable<Response> {
        this.fetching = true;
        return this.http.get(this.urlPrefix + '/api/insurance/get?search=' + term)
        .map(this.extractData)
        .catch(this.handleError);
    }

    getThisInsurance(id: number) {
        return this.http.get(this.urlPrefix + '/api/insurance/get?showId=' + id)
        .map(this.extractData)
        .catch(this.handleError);
    }

    searchInsurance(terms: Observable<string>) {
        return terms.debounceTime(600)
        .distinctUntilChanged()
        .switchMap(term => this.getInsurance(term));
    }

    getPolicyTypes(): void {
        let _self = this;
        this.http.get(this.urlPrefix + '/api/policy-types')
        .map(this.extractData)
        .toPromise()
        .then((data) => {
            this.policyTypes = _self.makeThisArray(data);
        })
        .catch(this.handleError);
    }

    getVehicleTypes(data): Observable<Response> {
        return this.http.post(this.urlPrefix + '/api/vehicle-types', data)
        .map(this.extractData)
        .catch(this.handleError);
    }

    getMvTypesAndPrice(data): Observable<Response> {
        return this.http.post(this.urlPrefix + '/api/mv-types-price', data)
        .map(this.extractData)
        .catch(this.handleError);
    }

    updateInsurance(data): Observable<Response> {
        return this.http.post(this.urlPrefix + '/api/insurance/update', data)
        .map(this.extractData)
        .catch(this.handleError);
    }

    createInsurance(data): Observable<Response> {
        return this.http.post(this.urlPrefix + '/api/insurance/save', data)
        .map(this.extractData)
        .catch(this.handleError);
    }

    doPayment(data: any) {

        let token: any = jQuery('meta[name="csrf-token"]').attr('content');
        let payUrl = environment.production ? dragonpay.live : dragonpay.test;
        let merchantKey = environment.production ? dragonpay.dr_live.key : dragonpay.dr_demo.key;
        let merchantId = environment.production ? dragonpay.dr_live.id : dragonpay.dr_demo.id;
        let description = 'Payment for insurance ' + data.insurance.mv_file_number;
        let shaSum = sha1(
            merchantId + ':' + data.insurance.transaction_reference + ':' + data.insurance.total_price + ':' +
            'PHP' + ':' + description + ':' + data.insurance.assured_email + ':' + merchantKey
        );

        // let newWindow = this.nativeWindow.open(
        //     payUrl + '?merchantId=' + merchantId
        //     + '&txnid=' + data.insurance.transaction_reference
        //     + '&amount=' + data.insurance.total_price
        //     + '&ccy=PHP'
        //     + '&description=' + description
        //     + '&email=' + data.insurance.assured_email
        //     + '&digest=' + shaSum
        //     + '&_token=' + token
        // );

        payUrl += '?merchantId=' + merchantId
            + '&txnid=' + data.insurance.transaction_reference
            + '&amount=' + data.insurance.total_price
            + '&ccy=PHP'
            + '&description=' + description
            + '&email=' + data.insurance.assured_email
            + '&digest=' + shaSum
            + '&param1=' + token;

        return payUrl;
    }

    cocafRegister(passData): void {
        this.http.post(this.urlPrefix + '/api/insurance/cocaf/register', passData)
        .map(this.extractData)
        .toPromise()
        .then((data) => { })
        .catch(this.handleError);
    }

    getBaseUri() {
        return jQuery('base').attr('href');
    }

    doPrint(key: string, dataId: number) {
        return this.urlPrefix + '/api/insurance/' + dataId + ' /print/' + key;

        // return this.http.get()
        //                 .map(this.extractData)
        //                 .catch(this.handleError);
    }

    downloadFile(url) {
        return this.http.get(url, this.downloadHeaders)
        .map(this.extractData)
        .catch(this.handleError);
    }
}
