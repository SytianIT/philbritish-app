import { TestBed, inject } from '@angular/core/testing';

import { MyTplService } from './my-tpl.service';

describe('MyTplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MyTplService]
    });
  });

  it('should be created', inject([MyTplService], (service: MyTplService) => {
    expect(service).toBeTruthy();
  }));
});
