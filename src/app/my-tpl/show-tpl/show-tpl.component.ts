import { AuthService } from 'app/auth.service';
import { setTimeout } from 'timers';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { MyTplService } from './../my-tpl.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AbstractComponent } from 'app/abstract.component';
import { Location } from '@angular/common';
import { MatMenuTrigger } from '@angular/material';

import { ISubscription } from 'rxjs/Subscription';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { PusherService } from './../../pusher.service';
import { SnackbarService } from './../../shared/snackbar.service';

@Component({
    selector: 'app-show-tpl',
    templateUrl: './show-tpl.component.html',
    styleUrls: ['./show-tpl.component.css']
})
export class ShowTplComponent extends AbstractComponent implements OnInit, OnDestroy {

    private subscription: ISubscription;

    @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
    @ViewChild('printingModal') public printingModal: ModalDirective;

    mobHeight: any;
    insuranceId: any;
    insuranceData: any = [];
    isUnpaid: any = false;
    idealHeight = 'auto';

    hasAuth = false;
    authData: any = [];

    htmlPrint: string;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private myTplService: MyTplService,
        private location: Location,
        private pusherService: PusherService,
        private snackbarService: SnackbarService,
        private authService: AuthService
    ) {
        super();
        this.authService.isLoggedInConfirmed$.subscribe(a => {
            this.hasAuth = a;
        });

        this.authService.isUserData$.subscribe(a => {
            this.authData = a;
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.pusherService.paymentChannel.unbind();
    }

    ngOnInit() {
        this.route.paramMap.subscribe((params) => {
            this.insuranceId = params.get('id');
        });

        this.subscription = this.myTplService.getThisInsurance(this.insuranceId)
            .subscribe(data => {
                this.insuranceData = data;
                if (this.insuranceData.payment.status == 'P') {
                    this.isUnpaid = true;

                    this.pusherService.paymentChannel.bind('paymentPostBack', (data: any) => {
                        if (data.id == this.authData.id) {
                            this.snackbarService.openSnackBar('Payment updated, page will refresh!', 'Got it!');
                            setTimeout(() => {
                                this.router.navigate(['app/account/my-tpl-insurance/show/', this.insuranceData.id]);
                            }, 500);
                        }
                    });

                    this.pusherService.paymentChannel.bind('paymentPostBackInvalid', (data: any) => {
                        if (data.id == this.authData.id) {
                            this.snackbarService.openSnackBar(data.message, 'Got it!');
                        }
                    });

                }
            }, err => {
                this.router.navigate(['app/account/my-tpl-insurance']);
            });
        this.mobHeight = (window.screen.height) - 280;
    }

    goBack() {
        this.location.back();
    }

    show() {
        this.printingModal.show();
    }

    hide() {
        this.printingModal.hide();
    }

    isSuccess() {
        return this.insuranceData.payment && this.insuranceData.payment.status == 'S' ? true : false;
    }

    isPending() {
        return this.insuranceData.payment && this.insuranceData.payment.status == 'P' ? true : false;
    }

    isFailed() {
        return this.insuranceData.payment
            && this.insuranceData.payment.status != 'P'
            && this.insuranceData.payment.status != 'S' ? true : false;
    }

    print(key: string) {
        this.htmlPrint = this.myTplService.doPrint(key, this.insuranceId);
        this.show();
    }

    goPrint() {
        const iFrame = window.frames['myFrame'];
        iFrame.focus();
        iFrame.print();
    }

    goRenew() {
        this.router.navigate(['app/account/my-tpl-insurance/reprocess/', this.insuranceData.id]);
    }

    // goRePurchase() {
    //     let data = {
    //         insurance: this.insuranceData
    //     }

    //     this.myTplService.doPayment(data);
    // }
}
