import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowTplComponent } from './show-tpl.component';

describe('ShowTplComponent', () => {
  let component: ShowTplComponent;
  let fixture: ComponentFixture<ShowTplComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowTplComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowTplComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
