import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyTplComponent } from './my-tpl.component';

describe('MyTplComponent', () => {
  let component: MyTplComponent;
  let fixture: ComponentFixture<MyTplComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyTplComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyTplComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
