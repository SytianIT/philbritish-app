import { ModalDirective } from 'ngx-bootstrap/modal';
import { MatMenuTrigger } from '@angular/material';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { AbstractComponent } from 'app/abstract.component';
import { SnackbarService } from './../shared/snackbar.service';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import {
    trigger,
    state,
    style,
    animate,
    transition,
    group
} from '@angular/animations';
import { MyTplService } from 'app/my-tpl/my-tpl.service';
import { ISubscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-my-tpl',
    templateUrl: './my-tpl.component.html',
    styleUrls: ['./my-tpl.component.css'],
    animations: [
        trigger('shrinkOut', [
            state('in', style({opacity : 1})),
            transition('* => void', [
                style({opacity: '*'}),
                animate('.3s ease', style({opacity: 0}))
            ]),
            transition('void => *', [
                style({opacity: '*'}),
                animate('.3s ease', style({opacity: '*'}))
            ]),
        ]),
    ]
})
export class MyTplComponent extends AbstractComponent implements OnInit, OnDestroy {

    @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
    @ViewChild('printingModal') public printingModal: ModalDirective;

    private subscription: ISubscription;

    insurance: any = [];

    searchTerm$ = new Subject<string>();
    fetchingDone = false;
    noRecords = false;

    activeId = null;
    mobHeight: any;
    idealHeight = 'auto';
    htmlPrint: string;

    constructor(
        private router: Router,
        public myTplService: MyTplService,
        private snackbarService: SnackbarService
    ) {
        super();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    ngOnInit() {
        this.fetchingDone = false;
        this.subscription = this.myTplService.searchInsurance(this.searchTerm$)
            .subscribe(data => {
                this.insurance = data;
                if (this.insurance.length == 0) {
                    this.noRecords = true;
                }
            }, (err) => {
                this.snackbarService.openSnackBar(err, 'Got it!')
            });
        this.searchTerm$.next('');
    }

    showList(action: boolean) {
        if (action) {
            this.fetchingDone = true;
            this.myTplService.fetching = false;
        } else {
            this.fetchingDone = false;
        }
    }

    goCreate() {
        this.router.navigate(['app/account/my-tpl-insurance/new']);
    }

    showInsurance(showInsurance) {
        if (showInsurance.status == 'incomplete') {
            this.router.navigate(['app/account/my-tpl-insurance/reprocess/', showInsurance.id]);
        } else {
            this.router.navigate(['app/account/my-tpl-insurance/show/', showInsurance.id]);
        }
    }

    show() {
        this.printingModal.show();
    }

    hide() {
        this.printingModal.hide();
    }

    showMenu(id: number) {
        this.activeId = id;
        setTimeout(() => {
            this.trigger.openMenu();
        }, 100);
    }

    print(key: string, id: number) {
        this.htmlPrint = this.myTplService.doPrint(key, id);
        this.show();
    }

    goPrint() {
        const iFrame = window.frames['myFrame'];
        iFrame.focus();
        iFrame.print();
    }
}
