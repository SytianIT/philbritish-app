import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { AppService } from './app.service';

@Injectable()
export class AuthService extends AppService {

    // userData: any = { name: '' };
    redirectUrl: string;
    hasLogged = false;

    public userData = new Subject<any>();
    isUserData$ = this.userData.asObservable();

    // isLoggedIn: boolean = false;
    public isLoggedIn = new Subject<boolean>();
    isLoggedInConfirmed$ = this.isLoggedIn.asObservable();

    private url = this.urlPrefix + '/api/auth';

    constructor(private http: Http) {
        super();
    }

    login(): Observable<Object> {
        return this.http.get(`${this.url}/user`)
            .map(this.extractData)
            .map((res) => {
                if (res.id) {
                    this.hasLogged = true;
                    // localStorage.setItem('auth_token', res.email);
                    return {'result' : true, 'data' : res};
                }

                return {'result' : false, 'data' : null};
                // return this.isLoggedIn;
            });
    }

    updateLoginState(data: boolean) {
        this.isLoggedIn.next(data);
    }

    updateUserData(data) {
        this.userData.next(data);
    }

    logout(): Observable<any> {
        return this.http.get(`${this.url}/logout`)
            .map(this.extractData)
            .map((res) => {
                // this.isLoggedIn = false;
                return res;
            });
    }

    authorize(creds: any): Promise<Object> {
        return this.http.post(`${this.url}/authorize`, creds)
                        .toPromise()
                        .then(this.extractData)
                        .catch(this.handlePromiseError);
    }

    goLogin(): string {
        return this.urlPrefix + '/auth/login';
    }

    getBaseUri() {
        return this.urlPrefix;
    }

    updateProfile(data: any): Observable<Object> {
        return this.http.post(this.urlPrefix + '/api/profile/update', data)
                        .map(this.extractData)
                        .catch(this.handleError);
    }
}
