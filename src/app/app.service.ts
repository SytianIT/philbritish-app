import { Injectable } from '@angular/core';
import { Headers, Response, Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import './rxjs-operators';
import { environment } from './../environments/environment.prod';

@Injectable()
export abstract class AppService {
    
    public urlPrefix = environment.production ? '/appie' : '/philbritish-ctpl/public/appie';
    // local host philbritish-ctpl/public/appie | Live /appie
    public headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    
    public extractData(res: Response) {
        const body = res.json();
        return body || [];
    }
    
    public handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        const errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        
        // check if only validation error
        if (error.status === 400) {
            return Observable.throw(error.json());
        }
        
        // check for 401 status code
        // if (error.status === 401) {
        //     this.sessionTimeout.next(error.status);
        // }
        
        // console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }
    
    public handlePromiseError(error: any) {
        let messages: any;
        let body = error.json();
        body = body.data || body || {};
        
        // check for 401 status code
        // if (error.status === 401) {
        //     this.sessionTimeout.next(error.status);
        // }
        
        if (body.hasOwnProperty('messages')) {
            messages = body.messages;
            messages = Object.keys(messages).map(function(k) { return messages[k][0] });
        } else {
            let errMsg = (error.message) ? error.message : (error.status ? `${error.status} - ${error.statusText}` : 'Server error');
            messages = [errMsg];
        }
        return Promise.reject(messages[0]);
    }
    
    public inThisArray(array, key, value): boolean {
        let flag = false;
        
        array.forEach(element => {
            if (element.hasOwnProperty(key)) {
                flag = (element[key] == value) ? true : flag;
            }
        });
        
        return flag;
    }
    
    public makeThisArray(myObj) {
        let arr = Object.keys(myObj).map(function(k) {
            return myObj[k];
        });
        
        return arr;
    }
}
