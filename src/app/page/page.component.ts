import { DomSanitizer } from '@angular/platform-browser';
import { InquiryFormComponent } from './partials/inquiry-form/inquiry-form.component';
import { ClaimFormComponent } from './partials/claim-form/claim-form.component';
import { SnackbarService } from './../shared/snackbar.service';
import { SettingsService } from 'app/settings/settings.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import * as _ from 'lodash';
import { Location } from '@angular/common';
import { AfterViewInit, OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { routerTransition } from 'app/shared/router.animations';
import { ISubscription } from 'rxjs/Subscription';
import { AbstractComponent } from 'app/abstract.component';

@Component({
    templateUrl : './page.component.html',
    // animations: [routerTransition()],
    // host: {'[@routerTransition]': ''}
})
export class PageComponent extends AbstractComponent implements OnInit, AfterViewInit, OnDestroy {

    @ViewChild('claimForm') public claimForm: ClaimFormComponent;
    @ViewChild('inquiryForm') public inquiryForm: InquiryFormComponent;

    private subscription: ISubscription;

    slug = '';
    page = [];
    isClaimPage = false;
    isContactPage = false;
    isAboutUs = false;
    hasValues = false;
    prices: any;
    contactFooter: any;
    contactBg: any;
    contactSocials = [];
    isLoading = true;

    currentPage: any = {
        slug : '',
        title : '',
        content : '',
        in_menu : '',
        other_data : [],
        published : '',
    };
    currentContent;

    constructor (
        private settingsService: SettingsService,
        private route: ActivatedRoute,
        private location: Location,
        private formBuilder: FormBuilder,
        private snackbarService: SnackbarService,
        private sanitized: DomSanitizer
    ) {
        super();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    ngOnInit() {
        this.isLoading = true;
        this.currentPage = {
            slug : '',
            title : '',
            content : '',
            in_menu : '',
            other_data : [],
            published : '',
        };

        this.route.paramMap.subscribe((params) => {
            this.isClaimPage = false;
            this.isContactPage = false;
            this.isAboutUs = false;
            this.slug = params.get('slug');

            if (this.slug == 'how-to-claim') {
                this.isClaimPage = true;
            }

            if (this.slug == 'contact-us') {
                this.isContactPage = true;
                this.settingsService.getContactContent()
                    .subscribe(data => {
                        this.contactFooter = data;
                        this.contactSocials = this.makeThisArray(data.socials);
                        this.contactBg = data.bg;
                    }, (err) => {
                        this.snackbarService.openSnackBar(err, 'Got it!')
                    });
            }

            if (this.slug == 'about-us') {
                this.isAboutUs = true;
                this.settingsService.getPremiumPrice()
                    .subscribe(data => {
                        this.prices = data;
                    }, (err) => {
                        this.snackbarService.openSnackBar(err, 'Got it!')
                    });
            }

            if (params.get('slug')) {
                this.subscription = this.settingsService.getThisPage(params.get('slug'))
                .subscribe(page => {
                    this.currentPage = page;
                    this.hasValues = true;
                    this.isLoading = false;
                });
            }

            this.getMetas(this.slug);
        });
    }

    ngAfterViewInit() { }

    onClaimSubmitted() { }

    onInquirySubmitted() { }

    getMetas(slug) {
        this.settingsService.getThisMeta(slug)
            .subscribe(data => { });
    }

    goBack() {
        this.location.back();
    }
}
