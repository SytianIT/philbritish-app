import { element } from 'protractor';
import { Injectable } from '@angular/core';
import { Headers, Response, Http, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { AppService } from 'app/app.service';

@Injectable()
export class InquiryService extends AppService {

    constructor(
        private http: Http
    ) {
        super();
    }

    sendInquiry(data: FormData): Observable<any> {
        return this.http.post(this.urlPrefix + '/api/inquiry/submit', data)
                        .map(this.extractData)
                        .catch(this.handleError);
    }
}
