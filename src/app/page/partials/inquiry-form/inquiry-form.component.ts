import { InquiryService } from 'app/page/partials/inquiry.service';
import { Component, OnInit, EventEmitter, Output, ElementRef, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { SnackbarService } from 'app/shared/snackbar.service';
import { RecaptchaComponent } from 'ng-recaptcha/recaptcha/recaptcha.component';
import {
    trigger,
    state,
    style,
    animate,
    transition
  } from '@angular/animations';

declare var jQuery: any;

@Component({
    selector: 'app-inquiry-form',
    templateUrl: './inquiry-form.component.html',
    styleUrls: ['./inquiry-form.component.css'],
    animations: [
        trigger('flyInOut', [
          state('in', style({transform: 'translateX(0)'})),
          transition('void => *', [
            style({transform: 'translateX(-100%)'}),
            animate(100)
          ]),
          transition('* => void', [
            animate(100, style({transform: 'translateX(100%)'}))
          ])
        ])
    ]
})
export class InquiryFormComponent implements OnInit {

    @Output() public inquirySubmitted = new EventEmitter<boolean>();
    @ViewChild('captchaRef') captchaRef: RecaptchaComponent;

    procText = 'Submit';
    isSent = false;
    isProcessing = false;
    inquiryForm: FormGroup;
    captchaKey = jQuery('meta[name="g-site-key"]').attr('content');
    captchaVerify = false;

    inquiryData: any = {
        first_name : '',
        last_name : '',
        contact_number : '',
        email : '',
        message : '',
    }

    constructor(
        private snackbarService: SnackbarService,
        private formBuilder: FormBuilder,
        private inquiryService: InquiryService
    ) { }

    ngOnInit() {
        this.inquiryForm = this.formBuilder
            .group({
                firstName: new FormControl('', [Validators.maxLength(50)]),
                lastName: new FormControl('', [Validators.maxLength(50)]),
                contactNumber: new FormControl('', [Validators.required, Validators.maxLength(15)]),
                email: new FormControl('', [Validators.required, Validators.maxLength(50), Validators.email]),
                message: new FormControl('', [Validators.required, Validators.maxLength(20)]),
            });
    }

    get firstName() { return this.inquiryForm.get('firstName'); };
    get lastName() { return this.inquiryForm.get('lastName'); };
    get contactNumber() { return this.inquiryForm.get('contactNumber'); };
    get email() { return this.inquiryForm.get('email'); };
    get message() { return this.inquiryForm.get('message'); };

    onSubmitInquiry() {
        if (this.inquiryForm.valid && this.isProcessing == false) {
            if (this.captchaVerify) {
                this.isProcessing = true;
                this.procText = 'Processing';
                this.inquiryService.sendInquiry(this.inquiryData)
                        .subscribe(
                            data => {
                                this.snackbarService.openSnackBar(data.message, 'Got it!');
                                this.resetData();
                            },
                            err => {
                                this.procText = 'Submit';
                                this.snackbarService.openSnackBar(err.messages, 'Got it!');
                                this.isProcessing = false;
                            }
                        );
            } else {
                alert('You need to confirm that you are not a robot!');
            }
        }
    }

    resetData () {
        this.inquiryData.first_name = '';
        this.inquiryData.last_name = '';
        this.inquiryData.contact_number = '';
        this.inquiryData.email = '';
        this.inquiryData.message = '';
        this.captchaRef.reset();
        this.isSent = true;
    }

    captchaResolved(captchaResponse: string) {
        if (captchaResponse !== null) {
            this.captchaVerify = true;
        } else {
            this.captchaVerify = false;
        }
    }
}
