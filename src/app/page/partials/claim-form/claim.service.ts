import { element } from 'protractor';
import { Injectable } from '@angular/core';
import { Headers, Response, Http, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { AppService } from 'app/app.service';

import * as _ from 'lodash';

@Injectable()
export class ClaimService extends AppService {

    public headers = new Headers({
        'Accept' : 'application/json'
    });
    claimHeaders = new RequestOptions({ headers: this.headers });

    constructor(
        private http: Http
    ) {
        super();
    }

    sendClaimRequest(data: FormData): Observable<any> {
        return this.http.post(this.urlPrefix + '/api/claim/submit', data, this.claimHeaders)
                        .map(this.extractData)
                        .catch(this.handleError);
    }

}
