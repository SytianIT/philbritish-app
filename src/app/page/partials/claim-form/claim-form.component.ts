import { RecaptchaComponent } from 'ng-recaptcha/recaptcha/recaptcha.component';
import { Component, OnInit, EventEmitter, Output, ElementRef, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { SnackbarService } from 'app/shared/snackbar.service';
import { ClaimService } from 'app/page/partials/claim-form/claim.service';
import {
    trigger,
    state,
    style,
    animate,
    transition
} from '@angular/animations';

declare var jQuery: any;

@Component({
    selector: 'app-claim-form',
    templateUrl: './claim-form.component.html',
    styleUrls: ['./claim-form.component.css'],
    animations: [
        trigger('flyInOut', [
          state('in', style({transform: 'translateX(0)'})),
          transition('void => *', [
            style({transform: 'translateX(-100%)'}),
            animate(100)
          ]),
          transition('* => void', [
            animate(100, style({transform: 'translateX(100%)'}))
          ])
        ])
    ]
})
export class ClaimFormComponent implements OnInit {

    @Output() public claimSubmitted = new EventEmitter<boolean>();
    @ViewChild('fileUploader') fileUploader: ElementRef;
    @ViewChild('captchaRef') captchaRef: RecaptchaComponent;

    procText = 'Submit';
    isSent = false;
    isProcessing = false;
    myFiles: any = [];
    formData = new FormData();
    howClaimForm: FormGroup;
    captchaKey = jQuery('meta[name="g-site-key"]').attr('content');
    captchaVerify = false;

    claimData: any = {
        holder_name : '',
        policy_number : '',
        contact_number: ''
    }

    constructor(
        private snackbarService: SnackbarService,
        private formBuilder: FormBuilder,
        private claimService: ClaimService
    ) { }

    ngOnInit() {
        this.howClaimForm = this.formBuilder
            .group({
                holderName: new FormControl('', [Validators.required, Validators.maxLength(50)]),
                policyNumber: new FormControl('', [Validators.required, Validators.maxLength(20)]),
                contactNumber: new FormControl('', [Validators.required, Validators.maxLength(15)]),
                // email: new FormControl('', [Validators.required, Validators.maxLength(50), Validators.email]),
                // message: new FormControl('', [Validators.required]),
            });
    }

    get HolderName() { return this.howClaimForm.get('holderName'); };
    get PolicyNumber() { return this.howClaimForm.get('policyNumber'); };
    get ContactNumber() { return this.howClaimForm.get('contactNumber'); };
    // get Email() { return this.howClaimForm.get('email'); };
    // get Message() { return this.howClaimForm.get('message'); };

    clickFileUpload() {
        this.fileUploader.nativeElement.click();
    }

    fileChange(event) {
        const _self = this;
        const fileList: Array<any> = event.target.files;
        Array.prototype.forEach.call(fileList, function(file) {
            _self.myFiles.push(file);
        });
    }

    removeFile(key) {
        if (key !== -1) {
            this.myFiles.splice(key, 1);
        }
    }

    resetFiles() {
        this.claimData.policy_number = '';
        this.claimData.holder_name = '';
        this.myFiles = [];
        this.isSent = true;
    }

    onSubmitClaim() {
        if (this.howClaimForm.valid) { //  && this.isProcessing == false
            if (this.myFiles.length > 0) {
                if (this.captchaVerify) {
                    this.procText = 'Processing';
                    this.isProcessing = true;
                    const formData = new FormData();
                    this.myFiles.forEach(element => {
                        formData.append('files[]', element);
                    });
                    formData.append('holder_name', this.claimData.holder_name);
                    formData.append('policy_number', this.claimData.policy_number);
                    formData.append('contact_number', this.claimData.contact_number);
                    this.claimService.sendClaimRequest(formData)
                        .subscribe(
                            data => {
                                this.snackbarService.openSnackBar(data.message, 'Got it!');
                                this.resetFiles();
                                this.captchaRef.reset();
                            },
                            err => {
                                this.procText = 'Submit';
                                this.snackbarService.openSnackBar(err.messages, 'Got it!');
                                this.isProcessing = false;
                            }
                        );
                } else {
                    alert('You need to confirm that you are not a robot!');
                }
            } else {
                this.snackbarService.openSnackBar(
                    'You have not provided any attachments, please provide one for faster transaction',
                    'Got it!'
                );
            }
        }
    }

    captchaResolved(captchaResponse: string) {
        if (captchaResponse !== null) {
            this.captchaVerify = true;
        } else {
            this.captchaVerify = false;
        }
    }
}
