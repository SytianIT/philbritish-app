// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const dragonpay = {
    test: 'http://test.dragonpay.ph/Pay.aspx',
    live: 'https://gw.dragonpay.ph/Pay.aspx',
    dr_demo : {
      id: 'PHILBRITISHASSURANCE',
      key: 'e4fnwyaKYzWcsuq'
    },
    dr_live : {
      id: 'PHILBRITISHASSURANCE',
      key: 'Hi4Cv7b4KqzhTsD'
    }
};

