import { PhilbritishAppPage } from './app.po';

describe('philbritish-app App', () => {
  let page: PhilbritishAppPage;

  beforeEach(() => {
    page = new PhilbritishAppPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
